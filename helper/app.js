// Version 1.1

document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'complete') {
      setTimeout(function(){

        $("#loading").addClass("animated fadeOut");
        $("#page_pc").addClass("animated bounceInDown");
        $("#page_tab_mobile").addClass("animated bounceInDown");


        // document.getElementById("loading").setAttribute("hidden", "");
        document.getElementById("page_pc").removeAttribute("hidden");
        document.getElementById("page_tab_mobile").removeAttribute("hidden");

      },1000);
  }
}






window.onload = function () {

  $('#authorInput').keypress(function(e){
       if(e.keyCode==13)
       $('#contactAuthorInput').focus();
  });
  $('#authorInputTab').keypress(function(e){
       if(e.keyCode==13)
       $('#contactAuthorInputTab').focus();
  });
  $('#contactAuthorInput').keypress(function(e){
       if(e.keyCode==13)
       $('#continuar_pc').click();
  });
  $('#contactAuthorInputTab').keypress(function(e){
       if(e.keyCode==13)
       $('#continue_tab').click();
  });

  var slectEnsino = document.getElementById("slectEnsino");
  var slectEnsinoTab = document.getElementById("slectEnsinoTab");

  // var slectEnsinoTab = document.getElementById("slectEnsinoTab").value;
  var ensinosObject = {"Ensino Superior":"John", "Ensino Secundário":"Doe"};



  // FOR PC OPTION
  for (var ensino in ensinosObject){
    slectEnsino.options[slectEnsino.options.length] = new Option (ensino, ensino);
  }
  slectEnsino.onchange = function () {
  if(slectEnsino.value == "Ensino Secundário"){
      //FOR POPULATING ENSINO SECUNDARIO PC:
      var slectClasse = document.getElementById("slectClasse"), //state / clsse
          slectDisc = document.getElementById("slectDisc"), //county / disciplina
          slectAno = document.getElementById("slectAno"); //city / anos

          document.getElementById('classeoption_pc').innerHTML="[selecione a Classe]"
          document.getElementById('disciplinaoption_pc').innerHTML="[selecione a Disciplina]"
          document.getElementById('anoepoca_pc').innerHTML="[selecione o Ano e Época]"

          slectClasse.length = 1;  // remove all options bar first
          slectDisc.length = 1; // remove all options bar first
          slectAno.length = 1; // remove all options bar first

      for (var classe in classeObject) {
          slectClasse.options[slectClasse.options.length] = new Option(classe, classe);
      }
      slectClasse.onchange = function () {
          slectDisc.length = 1; // remove all options bar first
          slectAno.length = 1; // remove all options bar first

          if (this.selectedIndex < 1) return; // done
          for (var disciplina in classeObject[this.value]) {
              slectDisc.options[slectDisc.options.length] = new Option(disciplina, disciplina);
          }
      };
      slectClasse.onchange(); // reset in case page is reloaded
      slectDisc.onchange = function () {
          slectAno.length = 1; // remove all options bar first
          if (this.selectedIndex < 1) return; // done
          var anos = classeObject[slectClasse.value][this.value];
          for (var i = 0; i < anos.length; i++) {
              slectAno.options[slectAno.options.length] = new Option(anos[i], anos[i]);
          }
      };

  } else if (slectEnsino.value == "Ensino Superior") {
  var selectUniversidade = document.getElementById("slectClasse"), //state / Universidade
      selectCadeira = document.getElementById("slectDisc"), //county / Cadeira
      selectAnoUni = document.getElementById("slectAno"); //city / Ano

      document.getElementById('classeoption_pc').innerHTML="[selecione a Universidade]"
      document.getElementById('disciplinaoption_pc').innerHTML="[selecione a Cadeira]"
      document.getElementById('anoepoca_pc').innerHTML="[selecione o Ano]"

      selectUniversidade.length = 1;  // remove all options bar first
      selectCadeira.length = 1; // remove all options bar first
      selectAnoUni.length = 1; // remove all options bar first

      for (var universidade in universidadeObject) {
          selectUniversidade.options[selectUniversidade.options.length] = new Option(universidade, universidade);
      }
      selectUniversidade.onchange = function () {
          selectCadeira.length = 1; // remove all options bar first
          selectAnoUni.length = 1; // remove all options bar first

          if (this.selectedIndex < 1) return; // done
          for (var cadeira in universidadeObject[this.value]) {
              selectCadeira.options[selectCadeira.options.length] = new Option(cadeira, cadeira);
          }
      };
      selectUniversidade.onchange(); // reset in case page is reloaded
      selectCadeira.onchange = function () {
          selectAnoUni.length = 1; // remove all options bar first
          if (this.selectedIndex < 1) return; // done
          var anos = universidadeObject[selectUniversidade.value][this.value];
          for (var i = 0; i < anos.length; i++) {
              selectAnoUni.options[selectAnoUni.options.length] = new Option(anos[i], anos[i]);
          }
      };
  }};



  // FOR MOBILE OPTION
  for (var ensinoTab in ensinosObject){
    slectEnsinoTab.options[slectEnsinoTab.options.length] = new Option (ensinoTab, ensinoTab);
  }
  slectEnsinoTab.onchange = function () {

    if(slectEnsinoTab.value == "Ensino Secundário"){

      var slectClasseTab = document.getElementById("slectClasseTab"), //state / clsse
          slectDiscTab = document.getElementById("slectDiscTab"), //county / disciplina
          slectAnoTab = document.getElementById("slectAnoTab"); //city / anos

          document.getElementById('classeoption_tab').innerHTML="[selecione a Classe]"
          document.getElementById('disciplinaoption_tab').innerHTML="[selecione a Disciplina]"
          document.getElementById('anooption_tab').innerHTML="[selecione o Ano e Época]"

          slectClasseTab.length = 1;  // remove all options bar first
          slectDiscTab.length = 1; // remove all options bar first
          slectAnoTab.length = 1; // remove all options bar first

      for (var classeTab in classeObject) {
          slectClasseTab.options[slectClasseTab.options.length] = new Option(classeTab, classeTab);
      }
      slectClasseTab.onchange = function () {
          slectDiscTab.length = 1; // remove all options bar first
          slectAnoTab.length = 1; // remove all options bar first

          if (this.selectedIndex < 1) return; // done
          for (var disciplinaTab in classeObject[this.value]) {
              slectDiscTab.options[slectDiscTab.options.length] = new Option(disciplinaTab, disciplinaTab);
          }
      };
      slectClasseTab.onchange(); // reset in case page is reloaded
      slectDiscTab.onchange = function () {
          slectAnoTab.length = 1; // remove all options bar first
          if (this.selectedIndex < 1) return; // done
          var anosTab = classeObject[slectClasseTab.value][this.value];
          for (var i = 0; i < anosTab.length; i++) {
              slectAnoTab.options[slectAnoTab.options.length] = new Option(anosTab[i], anosTab[i]);
          }
      };

    } else if (slectEnsinoTab.value == "Ensino Superior") {

      var selectUniversidadeTab = document.getElementById("slectClasseTab"), //state / Universidade
          selectCadeiraTab = document.getElementById("slectDiscTab"), //county / Cadeira
          selectAnoUniTab = document.getElementById("slectAnoTab"); //city / Ano

          document.getElementById('classeoption_tab').innerHTML="[selecione a Universidade]"
          document.getElementById('disciplinaoption_tab').innerHTML="[selecione a Cadeira]"
          document.getElementById('anooption_tab').innerHTML="[selecione o Ano]"

          selectUniversidadeTab.length = 1;  // remove all options bar first
          selectCadeiraTab.length = 1; // remove all options bar first
          selectAnoUniTab.length = 1; // remove all options bar first

          for (var universidadeTab in universidadeObject) {
              selectUniversidadeTab.options[selectUniversidadeTab.options.length] = new Option(universidadeTab, universidadeTab);
          }
              selectUniversidadeTab.onchange = function () {
              selectCadeiraTab.length = 1; // remove all options bar first
              selectAnoUniTab.length = 1; // remove all options bar first

              if (this.selectedIndex < 1) return; // done
              for (var cadeiraTab in universidadeObject[this.value]) {
                  selectCadeiraTab.options[selectCadeiraTab.options.length] = new Option(cadeiraTab, cadeiraTab);
              }
          };
          selectUniversidadeTab.onchange(); // reset in case page is reloaded
          selectCadeiraTab.onchange = function () {
              selectAnoUniTab.length = 1; // remove all options bar first
              if (this.selectedIndex < 1) return; // done
              var anosTab = universidadeObject[selectUniversidadeTab.value][this.value];
              for (var i = 0; i < anosTab.length; i++) {
                  selectAnoUniTab.options[selectAnoUniTab.options.length] = new Option(anosTab[i], anosTab[i]);
              }
          };
    }}

};//end onload



function isEmpty(str){
    return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
}

/*********************
     PC FUNCTIONS
**********************/
function continue_pc(){
  var aDisciplina = document.getElementById("slectDisc").value;
  var oAno = document.getElementById("slectAno").value;
  var aClasse = document.getElementById("slectClasse").value;
  var authorval = document.getElementById("authorInput").value;
  var contactauthval = document.getElementById("contactAuthorInput").value;
  var title = document.getElementById('filename');

  if((aClasse == "Universidade Pegógica (UP)") || (aClasse == "Universidade Eduardo Mondlane (UEM)") || (aClasse == "Inst. Sup. de Ciências de Saúde (ISCISA)") || (aClasse == "Inst. Sup. de Relações Internacionais (ISRI)")){
    var rarawUniversidade = document.getElementById("slectClasse").value;
    var rawUniversidade = rarawUniversidade.split("(");
    var splitRawUniv = rawUniversidade[1].split(")");
    var aClasse = splitRawUniv[0];

  } else {
    var aClasse = document.getElementById("slectClasse").value;
  }

  if (aClasse == "" || aDisciplina == "" || oAno == "" || isEmpty(authorval) || isEmpty(contactauthval)) {
      alert("Por favor selecione/preencha todos os dados para continuar.");
  } else {
    title.innerHTML = "[Guia Para] - " + aClasse + "-" + aDisciplina + "-" + oAno;

    document.getElementById('pc_tableconfiglayout').removeAttribute("hidden");
    document.getElementById('pc_tablelayout').removeAttribute("hidden");
    document.getElementById('filename').removeAttribute("hidden");

    $('#pc_infolayout').addClass('animated zoomOut');
    document.getElementById('pc_infolayout').setAttribute("hidden", "");
    $('#pc_introlayout').addClass('animated zoomOut');
    document.getElementById('pc_introlayout').setAttribute("hidden", "");
    $('#pc_tableconfiglayout').addClass('animated fadeIn');
    $('#pc_tablelayout').addClass('animated fadeIn');
  }
}

function goback(){

  document.getElementById('pc_tableconfiglayout').setAttribute("hidden", "");
  document.getElementById('pc_tablelayout').setAttribute("hidden", "");
  document.getElementById('pc_tableend').setAttribute("hidden", "");
  document.getElementById('filename').setAttribute("hidden", "");



  $('#pc_infolayout').removeClass('animated zoomOut');
  $('#pc_infolayout').addClass('animated fadeIn');
  document.getElementById('pc_infolayout').removeAttribute("hidden");
  $('#pc_introlayout').removeClass('animated zoomOut');
  $('#pc_introlayout').addClass('animated fadeIn');
  document.getElementById('pc_introlayout').removeAttribute("hidden");
  $('#pc_tableconfiglayout').removeClass('animated fadeIn');
  $('#pc_tableconfiglayout').addClass('animated fadeOut');
  $('#pc_tableconfiglayout').removeClass('animated fadeOut');
  $('#pc_tablelayout').removeClass('animated fadeIn');
  $('#pc_tablelayout').addClass('animated fadeOut');
  $('#pc_tablelayout').removeClass('animated fadeOut');
}

function getSelectedNames(){
  // FIRST GET WHAT'S SELECTED IN THE DROPDOWN AT THE MOMMENT
  var oNivel = document.getElementById("slectEnsino").value;
  var aClasse = document.getElementById("slectClasse").value;
  var aDisciplina = document.getElementById("slectDisc").value;
  var oAno = document.getElementById("slectAno").value;
}

function change(){
  var beginbtn = document.getElementById('addRow');
  var questionnumbersinput = document.getElementById('questionnumbersinput');
  var changeme = document.getElementById('changeme');
  var hint = document.getElementById('hinttext')
  var end = document.getElementById('save');
  var tableEnd = document.getElementById('pc_tableend');

  var whatiwantdisabled = document.getElementById('whatiwantdisabled');

  var conf = window.confirm("Deseja alterar o número de perguntas?\nNota: Se respondeste algumas questões e não submeteste, elas irão apagar-se.\n\nClique OK para alterar ou CANCEL para fechar esta janela.");

  if (conf == true){
    changeme.setAttribute('hidden', '');
    beginbtn.removeAttribute('hidden', '');
    hint.setAttribute('hidden', '');
    end.setAttribute('disabled', '');
    end.classList.remove("btn-unique");
    tableEnd.setAttribute('hidden', '');
    questionnumbersinput.removeAttribute('disabled', '');
    var row = document.getElementById('table').innerHTML="<thead><tr><th style='width: 100%' class='mdl-data-table__cell--non-numeric'>Nr. de Pergunta</th><th>Resposta</th></tr></thead>"
    whatiwantdisabled.classList.remove('disable_opacity');

  }
}

var editor = [];
function createTable(){
    //ELEMENTS FOR TABLE Config
    var beginbtn = document.getElementById('addRow');
    var questionnumbersinput = document.getElementById('questionnumbersinput');
    var changeme = document.getElementById('changeme');
    var hint = document.getElementById('hinttext');
    var end = document.getElementById('save');
    var tableEnd = document.getElementById('pc_tableend');

    var whatiwantdisabled = document.getElementById('whatiwantdisabled');

    var numb = questionnumbersinput.value;
    var notextplease = /^[0-9]+$/;

    if ((isEmpty(numb)) || (!numb.match(notextplease))) {
      $('#questionnumbersinput').addClass('animated pulse');
      alert('Por favor digite o número total de questões do exame que vais resolver.')

    } else {
      beginbtn.setAttribute('hidden', '');
      changeme.removeAttribute('hidden', '');
      hint.removeAttribute('hidden', '');

      questionnumbersinput.setAttribute('disabled', '');
      whatiwantdisabled.classList.add('disable_opacity');
      end.removeAttribute('disabled', '');
      end.classList.add("btn-unique");
      tableEnd.removeAttribute('hidden', '');


      var row = document.getElementById('table').innerHTML="<thead><tr><th class='mdl-data-table__cell--non-numeric'>Nr. de Pergunta</th><th style='width: 100%' class='mdl-data-table__cell--non-numeric'>Resposta</th></tr></thead>"
      var questionnumbers = document.getElementById('questionnumbersinput').value;
      for(var r = 1; r < parseInt(questionnumbers)+1; r++){
         var row = document.getElementById('table').insertRow(r);

         var cell1 = row.insertCell(0);
         cell1.classList.add('mdl-data-table__cell--non-numeric');
         var cell2 = row.insertCell(1);

         cell1.innerHTML = r;
         cell2.innerHTML = '<div style="max-width: 100%; width: 100%; border-left: 2px solid #303030; background-color: #f5f4f4;" id="editor' + r.toString() +'"></div>'

         // cell2.innerHTML = '<div style="width: 100%"><div id="editor' + r.toString() +'"></div>'

         var container = document.getElementById('editor' + r.toString());

         var container = document.getElementById('editor' + r.toString());
         editor[r] = new Quill(container, {
            placeholder: 'Ex: "A" ou "a)F". Leia as informações descritas acima.',
            readOnly: false,
            formats: [],
         });
      }
     }
}

var savee = [];
function run(){

  var ano = document.getElementById('slectAno').value;
  var classe = document.getElementById('slectClasse').value;
  var disciplina = document.getElementById('slectDisc').value;
  var uppercaseDisciplina = disciplina.toUpperCase();
  var table = document.getElementById('table');

  if((classe == "Universidade Pegógica (UP)") || (classe == "Universidade Eduardo Mondlane (UEM)") || (classe == "Inst. Sup. de Ciências de Saúde (ISCISA)") || (classe == "Inst. Sup. de Relações Internacionais (ISRI)")){
    var rarawUniversidade = document.getElementById("slectClasse").value;
    var rawUniversidade = rarawUniversidade.split("(");
    var splitRawUniv = rawUniversidade[1].split(")");
    var classe = splitRawUniv[0];
  } else {
    var classe = document.getElementById("slectClasse").value;
  }


  var send_button = document.getElementById('save');
  var logtext = document.getElementById('log_text');


  var autor = document.getElementById('authorInput').value;
  var autor_contact = document.getElementById('contactAuthorInput').value;
  // Number of questions:
  var qn = document.getElementById('questionnumbersinput').value;

  var confirmation = window.confirm("Deseja enviar?\n\nEste guia será enviado para a nossa equipe para ser feito um checkup e logo a seguir será colocado no site/app.\nMuitíssimo obrigado pela sua nobre contribuição.\n\nQualquer dúvida a nossa equipe irá lhe contactar.\nClique OK para enviar.");

  if (confirmation == true){

    send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Processando...'
    logtext.innerHTML='';


    var doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
    });

    var columns = ["Nº da pergunta", "Resposta"];
    var rows = createArray(qn, 2);

    for (i = 0; i < qn; i++){
      var resposta = editor[i+1].getText();
      rows[i][0] = i+1;
      rows[i][1] = resposta;
    }

    doc.setFontSize(12);
    doc.setFont('helvetica');
    doc.addImage(imgData, 'JPEG', 10, 1, 1.2, 1.2)
    doc.setFontStyle('bold');
    doc.text('- EXAMES MOÇAMBIQUE -', 10.5, 2.8, 'center');
    doc.setFontStyle('normal');
    doc.setFont('courier');
    doc.text('www.mozexames.com', 10.5, 3.4, 'center');
    doc.setFont('times');
    doc.text('Guia de correção facultado por ' + autor + '', 10.5, 4, 'center');
    doc.setFontSize(8);
    doc.setFontStyle('italic');
    doc.text('Notou alguma falha? Por favor contacte-nos para corrigir.', 10.5, 4.3, 'center');
    doc.setFontStyle('normal');
    doc.setFontSize(12);
    doc.text(ano + " / GUIA DE CORRECÇÃO DO EXAME DE " + uppercaseDisciplina + " / " + classe, 10.5, 5, 'center');

    doc.autoTable(columns, rows, {
      styles: {halign: 'left', font: 'times', lineWidth: 0.01, lineColor: 'black', overflow: 'linebreak'},
      showHeader: 'everyPage',
      theme: 'striped',
      tableLineWidth: 0,
      startY: 5.5,
      overflowColumns: false,
      margin: {top: 3.5},
      columnStyles: {
        0: {columnWidth: 'wrap'},
        1: {cellPadding: 0.1 ,columnWidth: 'auto'}}
    });

    var blob = doc.output('blob');
    var storageRef = firebase.storage().ref('Ajudas/' + classe + "-" + disciplina + "-" + ano + "(" + autor_contact + ")" + ".pdf");
    // Upload the file now to the storage
    var task = storageRef.put(blob);
    // When uploading, listen for
    task.on('state_changed',
          function progress(snapshot){
            var percentageRaw = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            var percentage = Math.round(percentageRaw * 100) / 100;

            send_button.setAttribute('disabled', '');
            send_button.classList.add("btn-unique");
            send_button.classList.remove("btn-danger");
            send_button.classList.remove("btn-success");



            logtext.innerHTML= 'Tamanho: ' + snapshot.totalBytes/1000 + 'KB' + '<br>Progresso: ' + percentage +' %' ;
            table.classList.add('disable_opacity');

            if(percentage < "100"){
              send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Enviando...'
            } else{
              send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Finalizando...'
            }


          },

          // Shit, an error happened while uploading, what is it?
          function (error){
            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
              case 'storage/unauthorized':
                // User doesn't have permission to access the object
                send_button.removeAttribute('disabled', '');
                send_button.classList.remove("btn-unique");
                send_button.classList.add("btn-danger");
                send_button.innerHTML='<i class="fa fa-exclamation-triangle fa-3x"></i> Tentar Novamente'
                logtext.innerHTML='Ocorreu um erro no upload: Permissão Negada<br>Contacte-nos'
                table.classList.remove('disable_opacity');
                break;

              case 'storage/canceled':
                // User canceled the upload
                console.log("CANCELED", error.code);

                break;

              case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                send_button.removeAttribute('disabled', '');
                send_button.classList.remove("btn-unique");
                send_button.classList.add("btn-danger");
                send_button.innerHTML='<i class="fa fa-exclamation-triangle fa-3x"></i> Tentar Novamente'
                logtext.innerHTML='Ocorreu um erro no upload: ' + error.serverResponse;
                table.classList.remove('disable_opacity');

                break;
            }
          },

          // Yayyy, the upload has completed successfully
          function complete(){
            console.log("COMPLETED!");
            table.classList.remove('disable_opacity');
            send_button.setAttribute('disabled', '');
            send_button.classList.remove("btn-unique");
            send_button.classList.add("btn-success");
            send_button.innerHTML='<i class="fa fa-check fa-3x"></i> Enviado com Sucesso!'
            logtext.innerHTML='<a onclick="see()">Clique para Ver o guia em PDF</a>'
          }
      );
  }
}

function see(){

    var ano = document.getElementById('slectAno').value;
    var classe = document.getElementById('slectClasse').value;
    var disciplina = document.getElementById('slectDisc').value.toUpperCase();

    var autor = document.getElementById('authorInput').value;
    var autor_contact = document.getElementById('contactAuthorInput').value;
    // Number of questions:
    var qn = document.getElementById('questionnumbersinput').value;

    var doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
    });

    var columns = ["Nº", "Resposta"];
    var rows = createArray(qn, 2);

    for (i = 0; i < qn; i++){
      var resposta = editor[i+1].getText();
      rows[i][0] = i+1;
      rows[i][1] = resposta;
    }

    doc.setFontSize(12);
    doc.setFont('helvetica');
    doc.addImage(imgData, 'JPEG', 10, 1, 1.2, 1.2)
    doc.setFontStyle('bold');
    doc.text('- EXAMES MOÇAMBIQUE -', 10.5, 2.8, 'center');
    doc.setFontStyle('normal');
    doc.setFont('courier');
    doc.text('www.mozexames.com', 10.5, 3.4, 'center');
    doc.setFont('times');
    doc.text('Guia de correção facultado por ' + autor + '', 10.5, 4, 'center');
    doc.setFontSize(8);
    doc.setFontStyle('italic');
    doc.text('Notou alguma falha? Por favor contacte-nos para corrigir.', 10.5, 4.3, 'center');
    doc.setFontStyle('normal');
    doc.setFontSize(12);
    doc.text(ano + " / GUIA DE CORRECÇÃO DO EXAME DE " + disciplina + " / " + classe, 10.5, 5, 'center');

    doc.autoTable(columns, rows, {
      styles: {halign: 'left', font: 'times', lineWidth: 0.01, lineColor: 'black', overflow: 'linebreak'},
      showHeader: 'everyPage',
      theme: 'striped',
      tableLineWidth: 0,
      startY: 5.5,
      overflowColumns: false,
      margin: {top: 3.5},
      columnStyles: {
        0: {columnWidth: 'wrap'},
        1: {cellPadding: 0.1 ,columnWidth: 'auto'}}
    });

    // doc.autoTable(columns, rows, {
    //     startY: 5.5,
    //     margin: {horizontal: 7},
    //     bodyStyles: {valign: 'top'},
    //     styles: {overflow: 'linebreak', columnWidth: 'wrap'},
    //     columnStyles: {text: {columnWidth: 'auto'}}
    // });

    doc.save(classe + "-" + disciplina + "-" + ano + "(" + autor_contact + ")" + ".pdf");


}
// END OF PC FUNCTIONS


// ------------------------- //



/*********************
TAB & MOBILE FUNCTIONS
**********************/
function continue_tab(){
  var aClasse = document.getElementById("slectClasseTab").value;
  var aDisciplina = document.getElementById("slectDiscTab").value;
  var oAno = document.getElementById("slectAnoTab").value;
  var authorval = document.getElementById("authorInputTab").value;
  var contactauthval = document.getElementById("contactAuthorInputTab").value;
  var title = document.getElementById('filenameTab');

  if((aClasse == "Universidade Pegógica (UP)") || (aClasse == "Universidade Eduardo Mondlane (UEM)") || (aClasse == "Inst. Sup. de Ciências de Saúde (ISCISA)") || (aClasse == "Inst. Sup. de Relações Internacionais (ISRI)")){
    var rarawUniversidade = document.getElementById("slectClasseTab").value;
    var rawUniversidade = rarawUniversidade.split("(");
    var splitRawUniv = rawUniversidade[1].split(")");
    var aClasse = splitRawUniv[0];
  } else {
    var aClasse = document.getElementById("slectClasseTab").value;
  }

  if (aClasse == "" || aDisciplina == "" || oAno == "" || isEmpty(authorval) || isEmpty(contactauthval)) {
      alert("Por favor selecione/preencha todos os dados para continuar.");
  } else {
    title.innerHTML = "[Guia Para] - " + aClasse + "-" + aDisciplina + "-" + oAno;

    document.getElementById('tab_tableconfiglayout').removeAttribute("hidden");
    document.getElementById('tab_tablelayout').removeAttribute("hidden");
    document.getElementById('filenameTab').removeAttribute("hidden");

    $('#tab_infolayout').addClass('animated zoomOut');
    document.getElementById('tab_infolayout').setAttribute("hidden", "");
    $('#tab_introlayout').addClass('animated zoomOut');
    document.getElementById('tab_introlayout').setAttribute("hidden", "");
    $('#tab_tableconfiglayout').addClass('animated fadeIn');
    $('#tab_tablelayout').addClass('animated fadeIn');
    window.scroll(0, 0);
  }
}

function changeTab(){
  var beginbtn = document.getElementById('addRowTab');
  var questionnumbersinput = document.getElementById('questionnumbersinputTab');
  var changeme = document.getElementById('changemeTab');
  var hint = document.getElementById('hinttextTab')
  var end = document.getElementById('saveTab');
  var tableEnd = document.getElementById('tab_tableend');


  var whatiwantdisabled = document.getElementById('whatiwantdisabledTab');

  var conf = window.confirm("Deseja alterar o número de perguntas?\nNota: Se respondeste algumas questões e não submeteste, elas irão apagar-se.\n\nClique OK para alterar ou CANCEL para fechar esta janela.");

  if (conf == true){
    changeme.setAttribute('hidden', '');
    beginbtn.removeAttribute('hidden', '');
    hint.setAttribute('hidden', '');
    tableEnd.setAttribute('hidden', '');
    end.setAttribute('disabled', '');
    end.classList.remove("btn-unique");
    questionnumbersinput.removeAttribute('disabled', '');
    var row = document.getElementById('tableTab').innerHTML="<thead><tr><th class='mdl-data-table__cell--non-numeric'>Clique no botão 'Começar a Resolver' para continuar</th></tr></thead>"
    whatiwantdisabled.classList.remove('disable_opacity');
  }
}

var editorTab = [];
function createTableTab(){

    //ELEMENTS FOR TABLE Config
    var beginbtn = document.getElementById('addRowTab');
    var questionnumbersinput = document.getElementById('questionnumbersinputTab');
    var changeme = document.getElementById('changemeTab');
    var hint = document.getElementById('hinttextTab');
    var end = document.getElementById('saveTab');
    var tableEnd = document.getElementById('tab_tableend');


    var whatiwantdisabled = document.getElementById('whatiwantdisabledTab');

    var numb = questionnumbersinput.value;
    var notextplease = /^[0-9]+$/;

    if ((isEmpty(numb)) || (!numb.match(notextplease))) {
      $('#questionnumbersinputTab').addClass('animated pulse');
      alert('Por favor digite o número total de questões do exame que vais resolver.')

    } else {
      beginbtn.setAttribute('hidden', '');
      changeme.removeAttribute('hidden', '');
      hint.removeAttribute('hidden', '');
      tableEnd.removeAttribute('hidden', '');


      questionnumbersinput.setAttribute('disabled', '');
      whatiwantdisabled.classList.add('disable_opacity');
      end.removeAttribute('disabled', '');
      end.classList.add("btn-unique");

      var row = document.getElementById('tableTab').innerHTML="<thead><tr><th class='mdl-data-table__cell--non-numeric'>Nr.</th><th style='width: 100%' class='mdl-data-table__cell--non-numeric'>Resposta</th></tr></thead>"
      var questionnumbers = document.getElementById('questionnumbersinputTab').value;
      for(var r = 1; r < parseInt(questionnumbers)+1; r++){
         var row = document.getElementById('tableTab').insertRow(r);

         var cell1 = row.insertCell(0);
         cell1.classList.add('mdl-data-table__cell--non-numeric');
         var cell2 = row.insertCell(1);

          cell1.innerHTML = r;
          cell2.innerHTML = '<div style="width: 100%; border-left: 2px solid #303030; background-color: #f5f4f4;" id="editor' + r.toString() +'"></div>'

          var container = document.getElementById('editor' + r.toString());
          editorTab[r] = new Quill(container, {
             placeholder: 'Ex: "A" ou "a)F". Leia as informações descritas acima.',
             readOnly: false,
             formats: [],
          });
      }
    }
}

function gobackTab(){
  document.getElementById('tab_tableconfiglayout').setAttribute("hidden", "");
  document.getElementById('tab_tablelayout').setAttribute("hidden", "");
  document.getElementById('tab_tableend').setAttribute("hidden", "");
  document.getElementById('filenameTab').setAttribute("hidden", "");


  $('#tab_infolayout').removeClass('animated zoomOut');
  $('#tab_infolayout').addClass('animated fadeIn');
  document.getElementById('tab_infolayout').removeAttribute("hidden");
  $('#tab_introlayout').removeClass('animated zoomOut');
  $('#tab_introlayout').addClass('animated fadeIn');
  document.getElementById('tab_introlayout').removeAttribute("hidden");
  $('#tab_tableconfiglayout').removeClass('animated fadeIn');
  $('#tab_tableconfiglayout').addClass('animated fadeOut');
  $('#tab_tableconfiglayout').removeClass('animated fadeOut');
  $('#tab_tablelayout').removeClass('animated fadeIn');
  $('#tab_tablelayout').addClass('animated fadeOut');
  $('#tab_tablelayout').removeClass('animated fadeOut');
}

function getSelectedNamesTab(){
  // FIRST GET WHAT'S SELECTED IN THE DROPDOWN AT THE MOMMENT
  var oNivel = document.getElementById("slectEnsinoTab").value;
  var aClasse = document.getElementById("slectClasseTab").value;
  var aDisciplina = document.getElementById("slectDiscTab").value;
  var oAno = document.getElementById("slectAnoTab").value;
}

var saveeTab = [];
function runTab(){

  var ano = document.getElementById('slectAnoTab').value;
  var classe = document.getElementById('slectClasseTab').value;
  var disciplina = document.getElementById('slectDiscTab').value;
  var uppercaseDisciplina = disciplina.toUpperCase();
  var table = document.getElementById('tableTab');

  if((classe == "Universidade Pegógica (UP)") || (classe == "Universidade Eduardo Mondlane (UEM)") || (classe == "Inst. Sup. de Ciências de Saúde (ISCISA)") || (classe == "Inst. Sup. de Relações Internacionais (ISRI)")){
    var rarawUniversidade = document.getElementById("slectClasseTab").value;
    var rawUniversidade = rarawUniversidade.split("(");
    var splitRawUniv = rawUniversidade[1].split(")");
    var classe = splitRawUniv[0];
  } else {
    var classe = document.getElementById("slectClasseTab").value;
  }

  var send_button = document.getElementById('saveTab');
  var logtext = document.getElementById('log_textTab');


  var autor = document.getElementById('authorInputTab').value;
  var autor_contact = document.getElementById('contactAuthorInputTab').value;
  // Number of questions:
  var qn = document.getElementById('questionnumbersinputTab').value;

  var confirmation = window.confirm("Deseja enviar?\n\nEste guia será enviado para a nossa equipe para ser feito um checkup e logo a seguir será colocado no site/app.\nMuitíssimo obrigado pela sua nobre contribuição.\n\nQualquer dúvida a nossa equipe irá lhe contactar.\nClique OK para enviar.");


  if (confirmation == true){

    send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Processando...'
    logtext.innerHTML='';


    var doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
    });

    var columns = ["Nº da pergunta", "Resposta"];
    var rows = createArray(qn, 2);

    for (i = 0; i < qn; i++){
      var resposta = editorTab[i+1].getText();
      rows[i][0] = i+1;
      rows[i][1] = resposta;
    }

    doc.setFontSize(12);
    doc.setFont('helvetica');
    doc.addImage(imgData, 'JPEG', 10, 1, 1.2, 1.2)
    doc.setFontStyle('bold');
    doc.text('- EXAMES MOÇAMBIQUE -', 10.5, 2.8, 'center');
    doc.setFontStyle('normal');
    doc.setFont('courier');
    doc.text('www.mozexames.com', 10.5, 3.4, 'center');
    doc.setFont('times');
    doc.text('Guia de correção facultado por ' + autor + '', 10.5, 4, 'center');
    doc.setFontSize(8);
    doc.setFontStyle('italic');
    doc.text('Notou alguma falha? Por favor contacte-nos para corrigir.', 10.5, 4.3, 'center');
    doc.setFontStyle('normal');
    doc.setFontSize(12);
    doc.text(ano + " / GUIA DE CORRECÇÃO DO EXAME DE " + uppercaseDisciplina + " / " + classe, 10.5, 5, 'center');

    doc.autoTable(columns, rows, {
      styles: {halign: 'left', font: 'times', lineWidth: 0.01, lineColor: 'black', overflow: 'linebreak'},
      showHeader: 'everyPage',
      theme: 'striped',
      tableLineWidth: 0,
      startY: 5.5,
      overflowColumns: false,
      margin: {top: 3.5},
      columnStyles: {
        0: {columnWidth: 'wrap'},
        1: {cellPadding: 0.1 ,columnWidth: 'auto'}}
    });

    var blob = doc.output('blob');
    var storageRef = firebase.storage().ref('Ajudas/' + classe + "-" + disciplina + "-" + ano + "(" + autor_contact + ")" + ".pdf");
    // Upload the file now to the storage
    var task = storageRef.put(blob);
    // When uploading, listen for
    task.on('state_changed',
          function progress(snapshot){
            var percentageRaw = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            var percentage = Math.round(percentageRaw * 100) / 100;

            table.classList.add('disable_opacity');
            send_button.setAttribute('disabled', '');
            send_button.classList.add("btn-unique");
            send_button.classList.remove("btn-success");
            send_button.classList.remove("btn-danger");

            logtext.innerHTML= 'Tamanho: ' + snapshot.totalBytes/1000 + 'KB' + '<br>Progresso: ' + percentage +' %' ;

            if(percentage < "100"){
              send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Enviando...'
            } else{
              send_button.innerHTML='<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i> Finalizando...'
            }


          },

          // Shit, an error happened while uploading, what is it?
          function (error){
            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
              case 'storage/unauthorized':
                // User doesn't have permission to access the object
                table.classList.remove('disable_opacity');
                send_button.removeAttribute('disabled', '');
                send_button.classList.remove("btn-unique");
                send_button.classList.add("btn-danger");
                send_button.innerHTML='<i class="fa fa-exclamation-triangle fa-3x"></i> Tentar Novamente'
                logtext.innerHTML='Ocorreu um erro no upload: Permissão Negada<br>Contacte-nos'
                break;

              case 'storage/canceled':
                // User canceled the upload
                console.log("CANCELED", error.code);
                table.classList.remove('disable_opacity');


                break;

              case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                send_button.removeAttribute('disabled', '');
                send_button.classList.remove("btn-unique");
                send_button.classList.add("btn-danger");
                send_button.innerHTML='<i class="fa fa-exclamation-triangle fa-3x"></i> Tentar Novamente'
                logtext.innerHTML='Ocorreu um erro no upload: ' + error.serverResponse;
                table.classList.remove('disable_opacity');
                break;
            }
          },

          // Yayyy, the upload has completed successfully
          function complete(){
            table.classList.remove('disable_opacity');
            send_button.setAttribute('disabled', '');
            console.log("COMPLETED!");
            send_button.classList.remove("btn-unique");
            send_button.classList.add("btn-success");
            send_button.innerHTML='<i class="fa fa-check fa-3x"></i> Enviado com Sucesso!'
            logtext.innerHTML='<a onclick="see()">Clique para ver o guia em PDF</a>'
          }
      );
    }


}

function seeTab(){

    var ano = document.getElementById('slectAnoTab').value;
    var classe = document.getElementById('slectClasseTab').value;
    var disciplina = document.getElementById('slectDiscTab').value.toUpperCase();

    var autor = document.getElementById('authorInputTab').value;
    var autor_contact = document.getElementById('contactAuthorInputTab').value;
    // Number of questions:
    var qn = document.getElementById('questionnumbersinputTab').value;

    var doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
    });

    var columns = ["Nº da pergunta", "Resposta"];
    var rows = createArray(qn, 2);

    for (i = 0; i < qn; i++){
      var resposta = editorTab[i+1].getText();
      rows[i][0] = i+1;
      rows[i][1] = resposta;
    }

    doc.setFontSize(12);
    doc.setFont('helvetica');
    doc.addImage(imgData, 'JPEG', 10, 1, 1.2, 1.2)
    doc.setFontStyle('bold');
    doc.text('- EXAMES MOÇAMBIQUE -', 10.5, 2.8, 'center');
    doc.setFontStyle('normal');
    doc.setFont('courier');
    doc.text('www.mozexames.com', 10.5, 3.4, 'center');
    doc.setFont('times');
    doc.text('Guia de correção facultado por ' + autor + '', 10.5, 4, 'center');
    doc.setFontSize(8);
    doc.setFontStyle('italic');
    doc.text('Notou alguma falha? Por favor contacte-nos para corrigir.', 10.5, 4.3, 'center');
    doc.setFontStyle('normal');
    doc.setFontSize(12);
    doc.text(ano + " / GUIA DE CORRECÇÃO DO EXAME DE " + disciplina + " / " + classe, 10.5, 5, 'center');

    doc.autoTable(columns, rows, {
      styles: {halign: 'left', font: 'times', lineWidth: 0.01, lineColor: 'black', overflow: 'linebreak'},
      showHeader: 'everyPage',
      theme: 'striped',
      tableLineWidth: 0,
      startY: 5.5,
      overflowColumns: false,
      margin: {top: 3.5},
      columnStyles: {
        0: {columnWidth: 'wrap'},
        1: {cellPadding: 0.1 ,columnWidth: 'auto'}}
    });

    doc.save(classe + "-" + disciplina + "-" + ano + "(" + autor_contact + ")" + ".pdf");

}
// END OF TAB FUNCTIONS




/*********************
  TABLE GEN AI (.pdf)
**********************/

var activebutton;
function active(id){
  if(activebutton != null){
     document.getElementById(activebutton).style['background-color'] = '#eee';
     document.getElementById(activebutton).style['color'] = '#000';
  };
  document.getElementById(id).style['background-color'] = 'black';
  document.getElementById(id).style['color'] = 'white';
  activebutton = id;
}

function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}
