document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'complete') {

    var config = {
        apiKey: "AIzaSyBsqI-AGvHpvdaDyvMQhSJJE9Pt01VgN5c",
        storageBucket: "examesproject.appspot.com"
    };

    firebase.initializeApp(config);
  }
};


var anosDezList = document.getElementById('anosDez');
var anosDozeList = document.getElementById('anosDoze');
var anosDiv = document.getElementById('anosDiv');
anosDiv.style.display = "none";
var epocasDiv = document.getElementById('epocasDiv');


var loadingSpinner = document.getElementById('loadingSpinner');


function hideAllAnos(){
  anosDiv.style.display = "none";
}


function hideEverythingForClasse(){
  anosDiv.style.display = "none";
  epocasDiv.style.display = "none";
}



function hideEverythingButAnosDez(disciplina) {
  var li = document.getElementById('dezLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  anosDiv.style.display = "block";
  anosDezList.style.display = "block";
  anosDozeList.style.display = "none";

  epocasDiv.style.display = "none";

  var anos = classeObject['10a Classe'][disciplina];

  var btn = document.createElement("BUTTON");
  btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
  btn.setAttribute('onclick', 'hideEverythingForDisciplinas()');
  btn.innerHTML = " – ";
  li.appendChild(btn);

  for (var i = 0; i < anos.length; i++) {
    btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'getExame(this.innerHTML, "10a Classe", "' + disciplina + '")');
    btn.innerHTML = anos[i];
    li.appendChild(btn);
  }
}


function hideEverythingButAnosDoze(disciplina) {
  var li = document.getElementById('dozeLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  anosDiv.style.display = "block";
  anosDozeList.style.display = "block";
  anosDezList.style.display = "none";

  var anos = classeObject['12a Classe'][disciplina];
  for (var i = 0; i < anos.length; i++) {
    var btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'getExame(this.innerHTML, "12a Classe", "' + disciplina + '")');
    btn.innerHTML = anos[i];
    li.appendChild(btn);
  }
}


// var
var hasEnunciado1 = true;
var hasEnunciado2 = true;
var hasGuia1 = true;
var hasGuia2 = true;
var currentAno;
var currentClasse;
var currentDisciplina;


function getExame(ano, classe, disciplina){
  hasEnunciado1 = true;
  hasEnunciado2 = true;
  hasGuia1 = true;
  hasGuia2 = true;

  currentAno = ano;
  currentClasse = classe;
  currentDisciplina = disciplina;

  console.log("debug: " + currentAno);
  console.log("debug: " + currentClasse);
  console.log("debug: " + currentDisciplina);

  var storage = firebase.storage();
  var storageRef = storage.ref();
  var epocaLi = document.getElementById('epocaLi');
  epocaLi.innerHTML = "";
  epocasDiv.style.display = "block";


  loadingSpinner.className = "uk-spinner uk-icon uk-animation-scale-down";
  epocasDiv.setAttribute('hidden', '');

  var enunciadoRef1a = storageRef.child(disciplina + "/" + classe + "/" + ano + "-" + "1a Epoca" + ".pdf");
  var enunciadoRef2a = storageRef.child(disciplina + "/" + classe + "/" + ano + "-" + "2a Epoca" + ".pdf");
  var guiaRef1a = storageRef.child("Guias/" + disciplina + "/" + classe + "/" + ano + "-" + "1a Epoca" + ".pdf");
  var guiaRef2a = storageRef.child("Guias/" + disciplina + "/" + classe + "/" + ano + "-" + "2a Epoca" + ".pdf");



  var promise1 = enunciadoRef1a.getDownloadURL().catch(function(reason){
    hasEnunciado1 = false;

  });
  var promise2 = enunciadoRef2a.getDownloadURL().catch(function(reason){
    hasEnunciado2 = false;
  });

  var promise3 = guiaRef1a.getDownloadURL().catch(function(reason){
    hasGuia1 = false;
  });
  var promise4 = guiaRef2a.getDownloadURL().catch(function(reason){
    hasGuia2 = false
  });

  Promise.all([promise1, promise2, promise3, promise4]).then(function(results) {
    epocaLi.innerHTML = "";
    loadingSpinner.className = "uk-hidden";

    for (var i = 0; i < 2; i++) { // For Enunciados Result
      if (results[i]) {
        var btn = document.createElement("BUTTON");
        btn.style.textTransform = "none"
        btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
        btn.innerHTML = i+1 + "a Epoca";
        btn.setAttribute('uk-toggle', "target: #modal-example");
        if (results[i+2]) {
          var epoca = i + 1;
          btn.setAttribute('onclick', "downloadBtns(" + epoca + " , '" + results[i] + "' , '" + results[i+2] + "')")

        } else {
          var epoca = i + 1;
          btn.setAttribute('onclick', "downloadBtns(" + epoca + ", '" + results[i] + "')")
        }
        epocaLi.appendChild(btn);
      }
    }

    epocasDiv.removeAttribute('hidden');

  }).catch(function(reason) {
    loadingSpinner.className = "uk-hidden";
    console.log(reason)
  });
}




function downloadBtns(epoca, enunciadolink, guialink = null){
  document.getElementById('modal_desc').innerHTML = "<i>" + currentDisciplina + " – " + currentAno + " – " + currentClasse + " – " + epoca + "ª Época" + "</i>"
  console.log(epoca);
  switch (epoca) {
    case 1:
      if (hasEnunciado1) {
        document.getElementById('enunciadobtn').classList.remove('uk-hidden');
        document.getElementById('enunciadobtn').href = enunciadolink;
        console.log(enunciadolink);
      } else {
        document.getElementById('enunciadobtn').classList.add('uk-hidden');
      }

      if (hasGuia1 && guialink) {
        document.getElementById('guiabtn').classList.remove('uk-hidden');
        document.getElementById('guiabtn').href = guialink;
      } else {
        document.getElementById('guiabtn').classList.add('uk-hidden');
      }

      break;

    case 2:
      if (hasEnunciado2) {
        document.getElementById('enunciadobtn').classList.remove('uk-hidden');
        document.getElementById('enunciadobtn').href = enunciadolink;

      } else {
        document.getElementById('enunciadobtn').classList.add('uk-hidden');
      }

      if (hasGuia2 && guialink) {
        document.getElementById('guiabtn').classList.remove('uk-hidden');
        document.getElementById('guiabtn').href = guialink;
      } else {
        document.getElementById('guiabtn').classList.add('uk-hidden');
      }
      break;
  }
}
