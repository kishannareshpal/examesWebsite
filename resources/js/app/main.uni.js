
var upAnosList = document.getElementById('anosUP');
var uemAnosList = document.getElementById('anosUEM');
var iscisaAnosList = document.getElementById('anosISCISA');
var isriAnosList = document.getElementById('anosISRI');

var uniAnosDiv = document.getElementById('uniAnosDiv');
uniAnosDiv.style.display = "none";
// var uniEpocasDiv = document.getElementById('uniEpocasDiv');

function uniHideAllAnos(){
  uniAnosDiv.style.display = "none";
}


function uniHideEverythingForUniversidade(){
  uniAnosDiv.style.display = "none";
}



function hideEverythingButAnosUP(disciplina) {
  var li = document.getElementById('upLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  uniAnosDiv.style.display = "block";
  upAnosList.style.display = "block";
  uemAnosList.style.display = "none";
  iscisaAnosList.style.display = "none";
  isriAnosList.style.display = "none";

  // uniEpocasDiv.style.display = "none";



  var uniAnos = universidadeObject['Universidade Pedagógica (UP)'][disciplina];

  for (var i = 0; i < uniAnos.length; i++) {
    btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'uniGetExame(this.innerHTML.split(" ")[0], "UP", "' + disciplina + '")');
    btn.innerHTML = uniAnos[i] + ' <div style="position: relative; bottom: 1.5px" id="' + "UP" + disciplina + "Progress_" + uniAnos[i] + '" class="uk-hidden" uk-spinner="ratio: .5"></div>';
    li.appendChild(btn);
  }
}


function hideEverythingButAnosUEM(disciplina) {
  var li = document.getElementById('uemLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  uniAnosDiv.style.display = "block";
  upAnosList.style.display = "none";
  uemAnosList.style.display = "block";
  iscisaAnosList.style.display = "none";
  isriAnosList.style.display = "none";

  // uniEpocasDiv.style.display = "none";

  var uniAnos = universidadeObject['Universidade Eduardo Mondlane (UEM)'][disciplina];

  for (var i = 0; i < uniAnos.length; i++) {
    btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'uniGetExame(this.innerHTML.split(" ")[0], "UEM", "' + disciplina + '")');
    btn.innerHTML = uniAnos[i] + ' <div style="position: relative; bottom: 1.5px" id="' + "UEM" + disciplina + "Progress_" + uniAnos[i] + '" class="uk-hidden" uk-spinner="ratio: .5"></div>';
    li.appendChild(btn);
  }
}


function hideEverythingButAnosISCISA(disciplina) {
  var li = document.getElementById('iscisaLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  uniAnosDiv.style.display = "block";
  upAnosList.style.display = "none";
  uemAnosList.style.display = "none";
  iscisaAnosList.style.display = "block";
  isriAnosList.style.display = "none";

  // uniEpocasDiv.style.display = "none";

  var uniAnos = universidadeObject['Instituto Superior de Ciências de Saúde (ISCISA)'][disciplina];

  for (var i = 0; i < uniAnos.length; i++) {
    btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'uniGetExame(this.innerHTML.split(" ")[0], "ISCISA", "' + disciplina + '")');
    btn.innerHTML = uniAnos[i] + ' <div style="position: relative; bottom: 1.5px" id="' + "ISCISA" + disciplina + "Progress_" + uniAnos[i] + '" class="uk-hidden" uk-spinner="ratio: .5"></div>';
    li.appendChild(btn);
  }
}


function hideEverythingButAnosISRI(disciplina) {
  var li = document.getElementById('isriLi' + disciplina);
  li.innerHTML = ""; // clear old anos;

  uniAnosDiv.style.display = "block";
  upAnosList.style.display = "none";
  uemAnosList.style.display = "none";
  iscisaAnosList.style.display = "none";
  isriAnosList.style.display = "block";

  // uniEpocasDiv.style.display = "none";

  var uniAnos = universidadeObject['Instituto Superior de Relações Internacionais (ISRI)'][disciplina];

  for (var i = 0; i < uniAnos.length; i++) {
    btn = document.createElement("BUTTON");
    btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    btn.setAttribute('onclick', 'uniGetExame(this.innerHTML.split(" ")[0], "ISRI", "' + disciplina + '")');
    btn.innerHTML = uniAnos[i] + ' <div style="position: relative; bottom: 1.5px" id="' + "ISRI" + disciplina + "Progress_" + uniAnos[i] + '" class="uk-hidden" uk-spinner="ratio: .5"></div>';
    li.appendChild(btn);
  }
}


// var
var uniHasGuia = true;
var uniHasEnunciado = true;

var currentAno;
var currentUni;
var currentDisciplina;

function uniGetExame(ano, universidade, disciplina){
  uniHasGuia = true;
  uniHasEnunciado = true;

  currentAno = ano;
  currentUni = universidade;

  switch (disciplina) {
    case "BiologiaParaEducacaoFisica":
      currentDisciplina = "Biologia para Educacao Fisica";
      break;

    case "Desenho1":
      currentDisciplina = "Desenho 1";
      break;

    case "Desenho2":
      currentDisciplina = "Desenho 2";
      break;

    case "Portugues1":
      currentDisciplina = "Portugues 1";
      break;

    case "Portugues2":
      currentDisciplina = "Portugues 2";
      break;

    default:
      currentDisciplina = disciplina;
      break;
  }

  var uniLoadingSpinner = document.getElementById(currentUni + disciplina + 'Progress_' + currentAno);
  console.log(currentUni + disciplina + 'Progress_' + currentAno);
  uniLoadingSpinner.className = "uk-spinner uk-text-primary uk-icon uk-margin-small-left";



  console.log("debug: " + currentAno);
  console.log("debug: " + currentUni);
  console.log("debug: " + currentDisciplina);

  var storageUniv = firebase.storage();
  var storageRefUniv = storageUniv.ref();
  // var uniEpocaLi = document.getElementById('uniEpocaLi');
  // uniEpocaLi.innerHTML = "";
  // uniEpocasDiv.style.display = "block";

  // uniEpocasDiv.setAttribute('hidden', '');

  var uniEnunciadoReference = storageRefUniv.child("Universidade/" + currentUni + "/" + currentDisciplina + "/" + currentUni + "-" + currentDisciplina + "-" + currentAno + ".pdf");
  var uniGuiaReference = storageRefUniv.child("Universidade/Guias/" + currentUni + "/" + currentDisciplina + "/" + currentUni + "-" + currentDisciplina + "-" + currentAno + "-Guia.pdf");


  var uniPromise1 = uniEnunciadoReference.getDownloadURL().catch(function(reason){
    uniHasEnunciado = false;

  });
  var uniPromise2 = uniGuiaReference.getDownloadURL().catch(function(reason){
    uniHasGuia = false;
  });

  Promise.all([uniPromise1, uniPromise2]).then(function(results) {
      // uniEpocaLi.innerHTML = "";
      uniLoadingSpinner.className = "uk-hidden";
      console.log(results);
      document.getElementById('uniModal_desc').innerHTML = "<i>" + currentUni + " – " + currentDisciplina + " – " + currentAno + "</i>"

      if (uniHasEnunciado) {
        document.getElementById('uniEnunciadobtn').classList.remove('uk-hidden');
        document.getElementById('uniEnunciadobtn').href = results[0];
      } else {
        document.getElementById('uniEnunciadobtn').classList.add('uk-hidden');
      }

      if (uniHasGuia && results[1]) {
        document.getElementById('uniGuiabtn').classList.remove('uk-hidden');
        document.getElementById('uniGuiabtn').href = results[1];
      } else {
        document.getElementById('uniGuiabtn').classList.add('uk-hidden');
      }

      uniLoadingSpinner.className = "uk-hidden";
      UIkit.modal('#uniModal').show();

    // for (var i = 0; i < 2; i++) { // For Enunciados Result
    //   if (results[i]) {
    //     var btn = document.createElement("BUTTON");
    //     btn.style.textTransform = "none"
    //     btn.className = "uk-text-bold uk-button-small uk-button uk-button-default uk-border-rounded uk-margin-small-bottom uk-margin-small-right";
    //     btn.innerHTML = i+1 + "a Epoca";
    //     btn.setAttribute('uk-toggle', "target: #uniModal");
    //     if (results[i+2]) {
    //       var epoca = i + 1;
    //       btn.setAttribute('onclick', "uniDownloadBtns(" + epoca + " , '" + results[i] + "' , '" + results[i+2] + "')")
    //
    //     } else {
    //       var epoca = i + 1;
    //       btn.setAttribute('onclick', "downloadBtns(" + epoca + ", '" + results[i] + "')")
    //     }
    //     // uniEpocaLi.appendChild(btn);
    //   }
    // }

    // uniEpocasDiv.removeAttribute('hidden');

  }).catch(function(reason) {
    uniLoadingSpinner.className = "uk-hidden";
    console.log(reason)
  });
}
