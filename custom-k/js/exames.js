
//END OF classeObject().

// Get the ENUNCIADO AND GUIA buttons:
var enunciadoButton = document.getElementById('enunciadoAnchor'); 
var guiaButton = document.getElementById('guiaAnchor');
var errorMessage = document.getElementById('error_message');

  window.onload = function () {
      //SOME ANIMATIONS ON PAGE LOAD
      //CARD TITLE SLIDES-UP FROM THE BOTTOM
      $('#cTitle').transition("jiggle");

      var slectClasse = document.getElementById("slectClasse"), //state / clsse
          slectDisc = document.getElementById("slectDisc"), //county / disciplina
          slectAno = document.getElementById("slectAno"); //city / anos
      
      for (var classe in classeObject) {
          slectClasse.options[slectClasse.options.length] = new Option(classe, classe);
      }
      slectClasse.onchange = function () {
          slectDisc.length = 1; // remove all options bar first
          slectAno.length = 1; // remove all options bar first

          if (this.selectedIndex < 1) return; // done
          for (var disciplina in classeObject[this.value]) {
              slectDisc.options[slectDisc.options.length] = new Option(disciplina, disciplina);
          }
      };
      slectClasse.onchange(); // reset in case page is reloaded
      slectEpoca.onchange();
      slectDisc.onchange = function () {
          slectAno.length = 1; // remove all options bar first
          if (this.selectedIndex < 1) return; // done
          var anos = classeObject[slectClasse.value][this.value];
          for (var i = 0; i < anos.length; i++) {
              slectAno.options[slectAno.options.length] = new Option(anos[i], anos[i]);
          }
      };
      
      
      
      
      
  }

  // WHAT HAPPENS WHEN SOMEONE CHANGES A VALUE FROM ANY OF THE DROPDOWNS? THIS HAPPENS:
  function getSelectedNames(){
    // FIRST GET WHAT'S SELECTED IN THE DROPDOWN AT THE MOMMENT
    var aClasse = document.getElementById("slectClasse").value;
    var aDisciplina = document.getElementById("slectDisc").value;
    var oAno = document.getElementById("slectAno").value;
    var aEpoca = document.getElementById("slectEpoca").value;

    // FIREBASE REFERENCES:
    // FIRST LETS GET TO THE STORAGE AND REFERENCE IT THROUGH *storageRef*
    var storage = firebase.storage();
    var storageRef = storage.ref();

    // DISABLE ALL THE BUTTONS AND SHOW LOADING FIRST
      //SHOW LAODING:
      $('#guiaAnchor').addClass("disabled");
      $('#enunciadoAnchor').addClass("disabled");
      document.getElementById('loadingStuff').classList.add("is-loading");
      


    // AFTERWARDS SET A REFERENCE TO THE GUIA FILE PATH ON THE STORAGE
    var guiaReference = storageRef.child("Guias/" + aDisciplina + "/" + aClasse + "/" + oAno + "-" + aEpoca + ".pdf");
    //GET THE URL OF THE SELECTED GUIA FILE
    guiaReference.getDownloadURL().then(function(urlGuia) {
        // IF THE GUIA WAS FOUND THEN ENABLE THE RESPECTIVE BUTTON
        document.getElementById('loadingStuff').classList.remove("is-loading");
        $('#guiaAnchor').removeClass("disabled");
        $('#guiaAnchor').transition("pulse");
        // AND IMPLEMENT THE FILE LINK TO THAT BUTTON
        document.getElementById("guiaAnchor").href=urlGuia;
    }).catch(function(error) {
            // IF ENCOUNTERS AN ERROR SUCH AS *File Not Found* DISABLE, OR KEEP THE BUTTON DISABLED, REMOVE LOADING.
            $('#guiaAnchor').addClass("disabled");
            document.getElementById('loadingStuff').classList.remove("is-loading");
        
      });

    // NOW GET SET A REFERENCE TO THE ENUNCIADO FILE PATH ON THE STORAGE
    var enunciadoReference = storageRef.child(aDisciplina + "/" + aClasse + "/" + oAno + "-" + aEpoca + ".pdf");
    // GET THE URL LINK OF THE SELECTED ENUNCIADO FILE
    enunciadoReference.getDownloadURL().then(function(urlEnunciado) {
        // IF THE ENUNCIADO FILE WAS FOUND ON THE SERVER, ENABLE THE BUTTON, AND HIDE THE LOADING
        document.getElementById('loadingStuff').classList.remove("is-loading");
        $('#enunciadoAnchor').removeClass("disabled");
        $('#enunciadoAnchor').transition("pulse");
        // AND IMPLEMENT THE FILE LINK TO THAT BUTTON.
        document.getElementById("enunciadoAnchor").href=urlEnunciado;
    }).catch(function(error) {
        // IF ENCOUNTERS AN ERROR SUCH AS *File Not Found* DISABLE, OR KEEP THE BUTTON DISABLED, REMOVE LOADING, SHOW THE APPROPRIATE ERROR MESSAGE TO THE USER
        document.getElementById('loadingStuff').classList.remove("is-loading");
        $('#enunciadoAnchor').addClass("disabled");
      });
      
  }

  // FIREBASE INIT
  // Replace with your project's customized code snippet
  var config = {
    apiKey: "AIzaSyBsqI-AGvHpvdaDyvMQhSJJE9Pt01VgN5c",
    authDomain: "examesproject.firebaseapp.com",
    storageBucket: "examesproject.appspot.com"
  };
  firebase.initializeApp(config);


      // INFO BUTTON ACTION
      function info(){
        var x = document.getElementById('info_text').hasAttribute('hidden');
        if(x == false){
          document.getElementById('info_text').setAttribute('hidden', '');
        } else if (x === true) {
          document.getElementById('info_text').removeAttribute('hidden');
        }
      }
